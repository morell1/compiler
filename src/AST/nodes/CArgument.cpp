#include "CArgument.h"
#include "src/AST/visitors/IVisitor.h"


namespace AST
{

void CArgument::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

}
