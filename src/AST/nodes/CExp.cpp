#include "CExp.h"
#include "src/AST/visitors/IVisitor.h"


namespace AST
{

const std::map<CBinaryExp::TYPE, const std::string> CBinaryExp::TypeToStr
    = {{TYPE::PLUS, "+"},
       {TYPE::MINUS, "-"},
       {TYPE::MUL, "*"},
       {TYPE::LESS, "<"},
       {TYPE::AND, "&&"}};

void CBinaryExp::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

void CIndexExp::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

void CLengthExp::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

void CMethodCallExp::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

void CIntegerExp::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

void CBooleanConstExp::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

void CIdentifierExp::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

void CThisExp::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

void CNewArrExp::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

void CNewExp::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

void CNegationExp::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

void CParenthesesExp::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

}
