#pragma once
#include "ITreeNodes.h"
#include "CIdentifier.h"

#include <map>


namespace AST
{

class CBinaryExp : public IExp
{

 public:
    enum class TYPE
    {
        PLUS, MINUS, MUL, AND, LESS
    };
    const static std::map<TYPE, const std::string> TypeToStr;

    CBinaryExp( TYPE type_, IExp* op1_, IExp* op2_ ) : type( type_ ), op1( op1_ ), op2( op2_ )
    {
    }

    void Accept( IVisitor* v ) const override;

    const std::string& GetTypeStr() const
    {
        return TypeToStr.at( type );
    }

    const IExp* GetLeft() const
    {
        return op1.get();
    }

    const IExp* GetRight() const
    {
        return op2.get();
    }

    const TYPE GetType() const
    {
        return type;
    }

 private:
    TYPE type;
    std::unique_ptr<IExp> op1;
    std::unique_ptr<IExp> op2;
};


class CIndexExp : public IExp
{
 public:
    CIndexExp( IExp* base_, IExp* index_ ) : base( base_ ), index( index_ )
    {
    }

    void Accept( IVisitor* v ) const override;

    const IExp* GetBase() const
    {
        return base.get();
    }

    const IExp* GetIndex() const
    {
        return index.get();
    }

 private:
    std::unique_ptr<IExp> base;
    std::unique_ptr<IExp> index;
};


class CLengthExp : public IExp
{
 public:
    explicit CLengthExp( IExp* expr_ ) : expr( expr_ )
    {
    }

    void Accept( IVisitor* v ) const override;

    const IExp* GetExp() const
    {
        return expr.get();
    }

 private:
    std::unique_ptr<IExp> expr;
};


class CMethodCallExp : public IExp
{
 public:
    CMethodCallExp( IExp* caller_, CIdentifier* funcName_,
                    std::vector<std::unique_ptr<IExp>>* args_ ) :
        caller( caller_ ), funcName( funcName_ ), args( args_ )
    {
    }

    void Accept( IVisitor* v ) const override;

    const IExp* GetCaller() const
    {
        return caller.get();
    }

    const CIdentifier* GetFuncName() const
    {
        return funcName.get();
    }

    const std::vector<std::unique_ptr<IExp>>* GetArgs() const
    {
        return args.get();
    }

 private:
    std::unique_ptr<IExp> caller;
    std::unique_ptr<CIdentifier> funcName;
    std::unique_ptr<std::vector<std::unique_ptr<IExp>>> args;
};


class CIntegerExp : public IExp
{
 public:
    explicit CIntegerExp( int value_ ) : value( value_ )
    {
    }

    void Accept( IVisitor* v ) const override;

    int GetValue() const
    {
        return value;
    }

 private:
    int value;
};


class CBooleanConstExp : public IExp
{
 public:
    explicit CBooleanConstExp( bool value_ ) : value( value_ )
    {
    }

    void Accept( IVisitor* v ) const override;

    bool GetValue() const
    {
        return value;
    }

 private:
    bool value;
};


class CIdentifierExp : public IExp
{
 public:
    explicit CIdentifierExp( CIdentifier* value_ ) : value( value_ )
    {
    }

    void Accept( IVisitor* v ) const override;

    const CIdentifier* GetValue() const
    {
        return value.get();
    }

 private:
    std::unique_ptr<CIdentifier> value;
};


class CThisExp : public IExp
{
 public:
    CThisExp() = default;

    void Accept( IVisitor* v ) const override;
};


class CNewArrExp : public IExp
{
 public:
    explicit CNewArrExp( IExp* length_ ) : length( length_ )
    {
    }

    void Accept( IVisitor* v ) const override;

    const IExp* GetLength() const
    {
        return length.get();
    }

 private:
    std::unique_ptr<IExp> length;
};


class CNewExp : public IExp
{
 public:
    explicit CNewExp( CIdentifier* type_ ) : type( type_ )
    {
    }

    void Accept( IVisitor* v ) const override;

    const CIdentifier* GetType() const
    {
        return type.get();
    }

 private:
    std::unique_ptr<CIdentifier> type;
};


class CNegationExp : public IExp
{
 public:
    explicit CNegationExp( IExp* op_ ) : op( op_ )
    {
    }

    void Accept( IVisitor* v ) const override;

    const IExp* GetOp() const
    {
        return op.get();
    }

 private:
    std::unique_ptr<IExp> op;
};


class CParenthesesExp : public IExp
{
 public:
    explicit CParenthesesExp( IExp* expr_ ) : expr( expr_ )
    {
    }

    void Accept( IVisitor* v ) const override;

    const IExp* GetExp() const
    {
        return expr.get();
    }

 private:
    std::unique_ptr<IExp> expr;
};

}
