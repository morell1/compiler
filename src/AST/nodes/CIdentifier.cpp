#include "CIdentifier.h"
#include "src/AST/visitors/IVisitor.h"


namespace AST
{

void CIdentifier::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

const CSymbol& CIdentifier::GetValue() const
{
    return value;
}

}
