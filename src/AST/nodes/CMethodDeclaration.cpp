#include "ITreeNodes.h"
#include "src/AST/visitors/IVisitor.h"


namespace AST
{

void CMethodDeclaration::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

CType* CMethodDeclaration::GetType() const
{
    return type.get();
}

const CIdentifier* CMethodDeclaration::GetId() const
{
    return id.get();
}

const std::vector<std::unique_ptr<CArgument>>* CMethodDeclaration::GetArguments() const
{
    return arguments.get();
}

const std::vector<std::unique_ptr<CVarDeclaration>>* CMethodDeclaration::GetLocalArguments() const
{
    return localArguments.get();
}

const std::vector<std::unique_ptr<IStm>>* CMethodDeclaration::GetStatements() const
{
    return statements.get();
}

const IExp* CMethodDeclaration::GetReturnExp() const
{
    return returnExp.get();
}

}