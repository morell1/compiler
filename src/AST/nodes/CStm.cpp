#include "CStm.h"
#include "src/AST/visitors/IVisitor.h"


namespace AST
{

void CIfStm::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

void CWhileStm::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

void CComplexStm::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

void CPrintStm::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

void CAssignmentStm::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

void CArrayAssignmentStm::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

}

