#pragma once

#include "ITreeNodes.h"
#include "CExp.h"
#include "CIdentifier.h"


namespace AST
{

class CIfStm : public IStm
{

 public:
    CIfStm() = delete;

    CIfStm( IExp* _exp, IStm* _ifStm, IStm* _elseStm ) :
        exp( _exp ), ifStm( _ifStm ), elseStm( _elseStm )
    {
    }

    void Accept( IVisitor* v ) const override;

    const IExp* GetExp() const
    {
        return exp.get();
    }

    const IStm* GetIfStm() const
    {
        return ifStm.get();
    }

    const IStm* GetElseStm() const
    {
        return elseStm.get();
    }

 private:
    std::unique_ptr<IExp> exp;
    std::unique_ptr<IStm> ifStm;
    std::unique_ptr<IStm> elseStm;
};


class CWhileStm : public IStm
{
 public:
    CWhileStm() = delete;

    CWhileStm( IExp* _exp, IStm* _stm ) :
        exp( _exp ),
        stm( _stm )
    {
    }

    void Accept( IVisitor* v ) const override;

    const IExp* GetExp() const
    {
        return exp.get();
    }

    const IStm* GetStm() const
    {
        return stm.get();
    }

 private:
    std::unique_ptr<IExp> exp;
    std::unique_ptr<IStm> stm;
};


class CComplexStm : public IStm
{
 public:
    CComplexStm() = delete;

    explicit CComplexStm( std::vector<std::unique_ptr<IStm>>* _statements ) : statements( _statements )
    {
    }

    void Accept( IVisitor* v ) const override;

    const std::vector<std::unique_ptr<IStm>>* GetStatements() const
    {
        return statements.get();
    }

 private:
    std::unique_ptr<std::vector<std::unique_ptr<IStm>>> statements;
};


class CPrintStm : public IStm
{
 public:
    CPrintStm() = delete;

    explicit CPrintStm( IExp* _exp ) : exp( _exp )
    {
    }

    void Accept( IVisitor* v ) const override;

    const IExp* GetExp() const
    {
        return exp.get();
    }

 private:
    std::unique_ptr<IExp> exp;
};


class CAssignmentStm : public IStm
{
 public:
    CAssignmentStm() = delete;

    CAssignmentStm( CIdentifierExp* _id, IExp* _exp ) : id( _id ), exp( _exp )
    {
    }

    void Accept( IVisitor* v ) const override;

    const CIdentifierExp* GetId() const
    {
        return id.get();
    }

    const IExp* GetExp() const
    {
        return exp.get();
    }

 private:
    std::unique_ptr<CIdentifierExp> id;
    std::unique_ptr<IExp> exp;
};

class CArrayAssignmentStm : public IStm
{
 public:
    CArrayAssignmentStm() = delete;

    CArrayAssignmentStm( CIdentifierExp* _id, IExp* _index, IExp* _result ) :
        id( _id ), index( _index ), result( _result )
    {
    }

    void Accept( IVisitor* v ) const override;

    const CIdentifierExp* GetId() const
    {
        return id.get();
    }

    const IExp* GetIndex() const
    {
        return index.get();
    }

    const IExp* GetResult() const
    {
        return result.get();
    }

 private:
    std::unique_ptr<CIdentifierExp> id;
    std::unique_ptr<IExp> index;
    std::unique_ptr<IExp> result;
};

}
