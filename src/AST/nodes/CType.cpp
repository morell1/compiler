#include "CType.h"
#include "src/AST/visitors/IVisitor.h"


namespace AST
{

CType& CType::operator=( const CType& other )
{
    type = other.type;
    name = std::unique_ptr<CIdentifier>( other.name.get() );
    return *this;
}

bool CType::operator==( const CType& other ) const
{
    return type == other.type && name.get() == other.name.get();
}

bool CType::operator!=( const CType& other ) const
{
    return !operator==( other );
}

const std::string CType::GetString() const
{
    switch( type ) {
        case CType::TYPE::INT_ARR:
            return "int[]";
        case CType::TYPE::INT:
            return "int";
        case CType::TYPE::BOOLEAN:
            return "boolean";
        case CType::TYPE::CUSTOM:
            return name->GetValue()->String();
        case CType::TYPE::UNKNOWN:
            return "UNKNOWN";
        case CType::TYPE::STR_ARR:
            return "String[]";
        case CType::TYPE::VOID:
            return "void";
        default:
            assert( false );
    }
}

void CType::Accept( IVisitor* v ) const
{
    v->Visit( this );
}

CType::TYPE CType::GetType() const
{
    return type;
}

const CIdentifier* CType::GetName() const
{
    return name.get();
}

}
