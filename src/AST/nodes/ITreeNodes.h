#pragma once
#include <string>
#include <vector>
#include <memory>
#include "src/SymbolTable/CSymbol.h"


namespace AST
{

class IVisitor;

class ITreeNode
{

 public:
    int fstCol;
    int fstLine;
    int lastCol;
    int lastLine;

    virtual void Accept( IVisitor* v ) const = 0;
};

class IStm : public ITreeNode
{
 public:
    virtual ~IStm() = default;
};

class IExp : public ITreeNode
{
 public:
    virtual ~IExp() = default;
};

}
using namespace AST;
