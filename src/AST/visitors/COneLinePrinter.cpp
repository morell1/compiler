#include <src/AST/nodes/CArgument.h>
#include <src/common.h>
#include "COneLinePrinter.h"


namespace AST
{

void COneLinePrinter::Visit( const CArgument* n )
{
    n->GetType()->Accept( this );
    n->GetId()->Accept( this );
}

void COneLinePrinter::Visit( const CGoal* n )
{
    n->GetMainClass()->Accept( this );
    for( auto& classDecl : *n->GetClassDeclarations() ) {
        classDecl->Accept( this );
    }
}

void COneLinePrinter::Visit( const CMainClass* n )
{
    fprintf( f, "class " );
    n->GetId()->Accept( this );
    fprintf( f, " { public static void main(String[] " );
    n->GetArgsId()->Accept( this );
    fprintf( f, ") { " );
    n->GetStatement()->Accept( this );
    fprintf( f, "} } " );
}

void COneLinePrinter::Visit( const CClassDeclaration* n )
{
    fprintf( f, "class " );
    n->GetId()->Accept( this );
    if( n->GetExtend() ) {
        fprintf( f, " extends " );
        n->GetExtend()->Accept( this );
    }
    fprintf( f, " { " );
    for( auto& varDeclaration : *n->GetVarDeclarations() ) {
        varDeclaration->Accept( this );
    }
    for( auto& methodDeclaration : *n->GetMethodDeclarations() ) {
        methodDeclaration->Accept( this );
    }
    fprintf( f, "} " );
}

void COneLinePrinter::Visit( const CVarDeclaration* n )
{
    n->GetType()->Accept( this );
    n->GetId()->Accept( this );
    fprintf( f, "; " );
}

void COneLinePrinter::Visit( const CMethodDeclaration* n )
{
    fprintf( f, "public " );
    n->GetType()->Accept( this );
    n->GetId()->Accept( this );
    fprintf( f, "(" );
    for( int i = 0; i < n->GetArguments()->size(); i++ ) {
        ( *( n->GetArguments() ) )[i]->Accept( this );
        if( i < n->GetArguments()->size() - 1 ) {
            fprintf( f, ", " );
        }
    }
    fprintf( f, ") { " );
    for( auto& localArgument: *n->GetLocalArguments() ) {
        localArgument->Accept( this );
    }
    for( auto& statement: *n->GetStatements() ) {
        statement->Accept( this );
    }
    fprintf( f, "return " );
    n->GetReturnExp()->Accept( this );
    fprintf( f, "; } " );
}

void COneLinePrinter::Visit( const CType* n )
{
    fprintf( f, "%s", n->GetString().c_str() );
    fprintf( f, " " );
}

void COneLinePrinter::Visit( const CIfStm* n )
{
    fprintf( f, "if(" );
    n->GetExp()->Accept( this );
    fprintf( f, ") " );
    n->GetIfStm()->Accept( this );
    fprintf( f, "else " );
    n->GetElseStm()->Accept( this );
}

void COneLinePrinter::Visit( const CWhileStm* n )
{
    fprintf( f, "while(" );
    n->GetExp()->Accept( this );
    fprintf( f, ") " );
    n->GetStm()->Accept( this );
}

void COneLinePrinter::Visit( const CComplexStm* n )
{
    fprintf( f, "{ " );
    for( auto& statement: *n->GetStatements() ) {
        statement->Accept( this );
    }
    fprintf( f, "} " );
}

void COneLinePrinter::Visit( const CPrintStm* n )
{
    fprintf( f, "System.out.println(" );
    n->GetExp()->Accept( this );
    fprintf( f, "); " );
}

void COneLinePrinter::Visit( const CAssignmentStm* n )
{
    n->GetId()->Accept( this );
    fprintf( f, " = " );
    n->GetExp()->Accept( this );
    fprintf( f, "; " );
}

void COneLinePrinter::Visit( const CArrayAssignmentStm* n )
{
    n->GetId()->Accept( this );
    fprintf( f, "[" );
    n->GetIndex()->Accept( this );
    fprintf( f, "] = " );
    n->GetResult()->Accept( this );
    fprintf( f, "; " );
}

void COneLinePrinter::Visit( const CBinaryExp* n )
{
    n->GetLeft()->Accept( this );
    fprintf( f, " %s ", n->GetTypeStr().c_str() );
    n->GetRight()->Accept( this );
}

void COneLinePrinter::Visit( const CIndexExp* n )
{
    n->GetBase()->Accept( this );
    fprintf( f, "[" );
    n->GetIndex()->Accept( this );
    fprintf( f, "]" );
}

void COneLinePrinter::Visit( const CLengthExp* n )
{
    n->GetExp()->Accept( this );
    fprintf( f, ".length" );
}

void COneLinePrinter::Visit( const CMethodCallExp* n )
{
    n->GetCaller()->Accept( this );
    fprintf( f, "." );
    n->GetFuncName()->Accept( this );
    fprintf( f, "(" );
    for( int i = 0; i < n->GetArgs()->size(); ++i ) {
        ( *n->GetArgs() )[i]->Accept( this );
        if( i != n->GetArgs()->size() - 1 ) {
            fprintf( f, ", " );
        }
    }
    fprintf( f, ")" );
}

void COneLinePrinter::Visit( const CIntegerExp* n )
{
    fprintf( f, "%d", n->GetValue() );
}

void COneLinePrinter::Visit( const CBooleanConstExp* n )
{
    fprintf( f, "%s", n->GetValue() ? "true" : "false" );
}

void COneLinePrinter::Visit( const CIdentifierExp* n )
{
    n->GetValue()->Accept( this );
}

void COneLinePrinter::Visit( const CThisExp* n )
{
    fprintf( f, "this" );
}

void COneLinePrinter::Visit( const CNewArrExp* n )
{
    fprintf( f, "new int[" );
    n->GetLength()->Accept( this );
    fprintf( f, "]" );
}

void COneLinePrinter::Visit( const CNewExp* n )
{
    fprintf( f, "new " );
    n->GetType()->Accept( this );
    fprintf( f, "()" );
}

void COneLinePrinter::Visit( const CNegationExp* n )
{
    fprintf( f, "!" );
    n->GetOp()->Accept( this );
}

void COneLinePrinter::Visit( const CParenthesesExp* n )
{
    fprintf( f, "(" );
    n->GetExp()->Accept( this );
    fprintf( f, ")" );
}

void COneLinePrinter::Visit( const CIdentifier* n )
{
    fprintf( f, "%s", n->GetValue()->String().c_str() );
}

}
