#pragma once

#include "IVisitor.h"


namespace AST
{
class COneLinePrinter : public IVisitor
{

 public:
    explicit COneLinePrinter( FILE* outFile ) : f( outFile )
    {
    }

    void Visit( const CGoal* n ) override;
    void Visit( const CMainClass* n ) override;
    void Visit( const CClassDeclaration* n ) override;
    void Visit( const CVarDeclaration* n ) override;
    void Visit( const CArgument* n ) override;
    void Visit( const CMethodDeclaration* n ) override;

    void Visit( const CType* n ) override;

    void Visit( const CIfStm* n ) override;
    void Visit( const CWhileStm* n ) override;
    void Visit( const CComplexStm* n ) override;
    void Visit( const CPrintStm* n ) override;
    void Visit( const CAssignmentStm* n ) override;
    void Visit( const CArrayAssignmentStm* n ) override;

    void Visit( const CBinaryExp* n ) override;
    void Visit( const CIndexExp* n ) override;
    void Visit( const CLengthExp* n ) override;
    void Visit( const CMethodCallExp* n ) override;
    void Visit( const CIntegerExp* n ) override;
    void Visit( const CBooleanConstExp* n ) override;
    void Visit( const CIdentifierExp* n ) override;
    void Visit( const CThisExp* n ) override;
    void Visit( const CNewArrExp* n ) override;
    void Visit( const CNewExp* n ) override;
    void Visit( const CNegationExp* n ) override;
    void Visit( const CParenthesesExp* n ) override;

    void Visit( const CIdentifier* n ) override;

 private:
    FILE* f;
};
}
