#pragma once

#include "src/AST/nodes/CClassDeclaration.h"
#include "src/AST/nodes/CExp.h"
#include "src/AST/nodes/ITreeNodes.h"
#include <src/AST/nodes/CArgument.h>
#include <src/AST/nodes/CStm.h>
#include <src/AST/nodes/CType.h>
#include <src/AST/nodes/CVarDeclaration.h>
#include "src/AST/nodes/CGoal.h"
#include "src/AST/nodes/CMainClass.h"


namespace AST
{
class IVisitor
{

 public:

    virtual void Visit( const CGoal* n ) = 0;
    virtual void Visit( const CMainClass* n ) = 0;
    virtual void Visit( const CClassDeclaration* n ) = 0;
    virtual void Visit( const CVarDeclaration* n ) = 0;
    virtual void Visit( const CArgument* n ) = 0;
    virtual void Visit( const CMethodDeclaration* n ) = 0;

    virtual void Visit( const CType* n ) = 0;

    virtual void Visit( const CIfStm* n ) = 0;
    virtual void Visit( const CWhileStm* n ) = 0;
    virtual void Visit( const CComplexStm* n ) = 0;
    virtual void Visit( const CPrintStm* n ) = 0;
    virtual void Visit( const CAssignmentStm* n ) = 0;
    virtual void Visit( const CArrayAssignmentStm* n ) = 0;

    virtual void Visit( const CBinaryExp* n ) = 0;
    virtual void Visit( const CIndexExp* n ) = 0;
    virtual void Visit( const CLengthExp* n ) = 0;
    virtual void Visit( const CMethodCallExp* n ) = 0;
    virtual void Visit( const CIntegerExp* n ) = 0;
    virtual void Visit( const CBooleanConstExp* n ) = 0;
    virtual void Visit( const CIdentifierExp* n ) = 0;
    virtual void Visit( const CThisExp* n ) = 0;
    virtual void Visit( const CNewArrExp* n ) = 0;
    virtual void Visit( const CNewExp* n ) = 0;
    virtual void Visit( const CNegationExp* n ) = 0;
    virtual void Visit( const CParenthesesExp* n ) = 0;

    virtual void Visit( const CIdentifier* n ) = 0;
};

}
