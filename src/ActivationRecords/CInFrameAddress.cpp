#include "CInFrameAddress.h"


IRT::IExp* IRT::CInFrameAddress::ToExp() const
{
    const IExp* offsetExpression;
    if( offset != 0 ) {
        offsetExpression =
            new CBinaryExp(
                CBinaryExp::EBinaryType::PLUS,
                frameAddress->ToExp(),
                new CConstExp( offset )
            );
    } else {
        offsetExpression = frameAddress->ToExp();
    }
    return new CMemoryExp( offsetExpression );
}
