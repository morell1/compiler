#pragma once

#include <src/AST/nodes/CExp.h>
#include <src/IRT/nodes/CIRTExp.h>
#include "IAddress.h"


namespace IRT
{

class CInFrameAddress : public IAddress
{

 public:
    CInFrameAddress( const IAddress* address_, const int offset_ ) : frameAddress( address_ ), offset( offset_ )
    {
    }

    IExp* ToExp() const override;

 private:
    const IAddress* frameAddress;
    int offset;
};

}
