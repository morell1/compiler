#include "CX86MiniJavaFrame.h"


namespace IRT
{

const std::string CFrame::FP = "FP";
const std::string CFrame::THIS = "THIS";
const std::string CFrame::RET = "RET";

const IAddress* CFrame::GetAddress( const std::string& name )
{
    auto res = addresses.find( name );
    if( res != addresses.end() ) {
        return res->second.get();
    } else {
        return nullptr;
    }
}

const IExp* CFrame::ExternalCall( const std::string& functionName, const IExp* args ) const
{
    return new CCallExp( new CNameExp( CLabel( functionName ) ), new CExpList( args ) );
}

}
