#pragma once

#include "IFrame.h"


namespace IRT
{

class CFrame : public IFrame
{

 public:
    const static std::string FP;
    const static std::string THIS;
    const static std::string RET;

    CFrame( const CSymbol classSymbol, const CSymbol methodSymbol ) :
        name( classSymbol->String() + "::" + methodSymbol->String() ),
        frameSize( 0 )
    {
        AddAddress( FP, new CInRegAddress( CTemp( FP ) ) );
        AddAddress( THIS, new CInRegAddress( CTemp( THIS ) ) );
        AddAddress( RET, new CInRegAddress( CTemp( RET ) ) );
    }

    const std::string& GetName() const override
    {
        return name;
    }

    int WordSize() const override
    {
        return 4;
    }

    void AddFieldAddress( const std::string& name ) override
    {
        AddAddress( name, new CInFrameAddress( GetAddress( THIS ), frameSize ) );
        frameSize += WordSize();
    }

    void AddLocalAddress( const std::string& name ) override
    {
        AddAddress( name, new CInFrameAddress( GetAddress( FP ), frameSize ) );
        frameSize += WordSize();
    }

    void AddAddress( const std::string& name, const IAddress* address ) override
    {
        addresses[name] = std::unique_ptr<const IAddress>( address );
    }

    const IAddress* GetAddress( const std::string& name ) override;

    const IExp* ExternalCall( const std::string& functionName, const IExp* args ) const override;

 private:
    std::string name;
    std::map<std::string, std::unique_ptr<const IAddress>> addresses;
    int frameSize;
};

}
