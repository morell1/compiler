#pragma once

#include <src/IRT/nodes/IExp.h>


namespace IRT
{
class IAddress
{

 public:
    virtual ~IAddress() = default;
    virtual IExp* ToExp() const = 0;
};

}
