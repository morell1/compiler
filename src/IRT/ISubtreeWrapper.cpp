#include "ISubtreeWrapper.h"


namespace IRT
{

const IExp* CExpWrapper::ToExp() const
{
    return expr;
}

const IStm* CExpWrapper::ToStm() const
{
    return new CExpStm( expr );
}

const IStm* CExpWrapper::ToConditional( const CLabel& t, const CLabel& f ) const
{
    return new CCJumpStm( CCJumpStm::ERelationType::LT,
                          expr, static_cast<IExp*>(new CConstExp( 0 )), t, f );
}

const IExp* CStmtWrapper::ToExp() const
{
    assert( false );
}

const IStm* CStmtWrapper::ToStm() const
{
    return stmt;
}

const IStm* CStmtWrapper::ToConditional( const CLabel& t, const CLabel& f ) const
{
    assert( false );
}

const IExp* CCondWrapper::ToExp() const
{
    assert( false );
    CTempExp* tempExp = new CTempExp( CTemp() );
    CLabel labelFalse;
    CLabel labelTrue;
    return
        new CESeqExp(
            new CSeqStm(
                new CMoveStm( tempExp, new CConstExp( 1 ) ),
                new CSeqStm(
                    ToConditional( labelTrue, labelFalse ),
                    new CSeqStm(
                        new CLabelStm( labelFalse ),
                        new CSeqStm(
                            new CMoveStm( tempExp, new CConstExp( 0 ) ),
                            new CLabelStm( labelTrue )
                        )
                    )
                )
            ),
            tempExp
        );
}

const IStm* CCondWrapper::ToStm() const
{
    assert( false );
}

const IStm* CRelationConditionalWrapper::ToConditional( const CLabel& labelTrue, const CLabel& labelFalse ) const
{
    return
        new CCJumpStm(
            type,
            leftOp,
            rightOp,
            labelTrue,
            labelFalse
        );
}

const IStm* CAndConditionalWrapper::ToConditional( const CLabel& labelTrue, const CLabel& labelFalse ) const
{
    CLabel labelMiddle( "ANDMIDDLE", true );
    return
        new CSeqStm(
            leftOp->ToConditional( labelMiddle, labelFalse ),
            new CSeqStm(
                new CLabelStm( labelMiddle ),
                rightOp->ToConditional( labelTrue, labelFalse )
            )
        );
}

const IStm* CNegateConditionalWrapper::ToConditional( const CLabel& labelTrue, const CLabel& labelFalse ) const
{
    return wrapper->ToConditional( labelFalse, labelTrue ); // reversed order of arguments
}

}
