#pragma once
#include <cassert>
#include "src/IRT/nodes/IStm.h"
#include "src/IRT/nodes/IExp.h"
#include "src/IRT/nodes/CIRTExp.h"
#include "src/IRT/nodes/CIRTStm.h"


namespace IRT
{

class ISubtreeWrapper
{

 public:
    virtual ~ISubtreeWrapper() = default;

    virtual const IExp* ToExp() const = 0; // как Expr
    virtual const IStm* ToStm() const = 0; // как Stm
    virtual const IStm* ToConditional( const CLabel& t, const CLabel& f ) const = 0; // как if/jump
};

class CExpWrapper : public ISubtreeWrapper
{

 public:
    explicit CExpWrapper( const IExp* e ) : expr( e )
    {
    }

    const IExp* ToExp() const override;
    const IStm* ToStm() const override;
    const IStm* ToConditional( const CLabel& t, const CLabel& f ) const override;

 private:
    const IExp* expr;
};

class CStmtWrapper : public ISubtreeWrapper
{

 public:
    explicit CStmtWrapper( const IStm* s ) : stmt( s )
    {
    }

    const IExp* ToExp() const override;
    const IStm* ToStm() const override;
    const IStm* ToConditional( const CLabel& t, const CLabel& f ) const override;

 private:
    const IStm* stmt;
};

class CCondWrapper : public ISubtreeWrapper
{

 public:
    const IExp* ToExp() const override;
    const IStm* ToStm() const override;
    const IStm* ToConditional( const CLabel& t, const CLabel& f ) const override = 0;
};

class CRelationConditionalWrapper : public CCondWrapper
{

 public:
    CRelationConditionalWrapper( CCJumpStm::ERelationType type_, const IExp* leftOp_, const IExp* rightOp_ ) :
        type( type_ ), leftOp( leftOp_ ), rightOp( rightOp_ )
    {
    }

    const IStm* ToConditional( const CLabel& labelTrue, const CLabel& labelFalse ) const override;

 private:
    CCJumpStm::ERelationType type;
    const IExp* leftOp;
    const IExp* rightOp;
};

class CAndConditionalWrapper : public CCondWrapper
{

 public:
    CAndConditionalWrapper( const ISubtreeWrapper* leftOp_, const ISubtreeWrapper* rightOp_ ) :
        leftOp( leftOp_ ), rightOp( rightOp_ )
    {
    }

    const IStm* ToConditional( const CLabel& labelTrue, const CLabel& labelFalse ) const override;

 private:
    std::unique_ptr<const ISubtreeWrapper> leftOp;
    std::unique_ptr<const ISubtreeWrapper> rightOp;
};

class CNegateConditionalWrapper : public CCondWrapper
{

 public:
    explicit CNegateConditionalWrapper( ISubtreeWrapper* wrapper_ ) : wrapper( wrapper_ )
    {
    }

    const IStm* ToConditional( const CLabel& labelTrue, const CLabel& labelFalse ) const override;

 private:
    std::unique_ptr<ISubtreeWrapper> wrapper;
};

}
