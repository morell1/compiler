#include "CIRTExp.h"
#include "src/IRT/visitors/IVisitorIRT.h"


namespace IRT
{

void CConstExp::Accept( IVisitorIRT* v ) const
{
    v->Visit( this );
}

int CConstExp::GetValue() const
{
    return value;
}

void CNameExp::Accept( IVisitorIRT* v ) const
{
    v->Visit( this );
}

const CLabel& CNameExp::GetLabel() const
{
    return label;
}

void CTempExp::Accept( IVisitorIRT* v ) const
{
    v->Visit( this );
}

const std::string CTempExp::GetValueLabel() const
{
    return value.String();
}

const std::map<CBinaryExp::EBinaryType, const std::string> CBinaryExp::TypeToStr
    = {{EBinaryType::PLUS, "+"},
       {EBinaryType::MINUS, "-"},
       {EBinaryType::MULTIPLY, "*"},
       {EBinaryType::AND, "&&"}};

void CBinaryExp::Accept( IVisitorIRT* v ) const
{
    v->Visit( this );
}

const std::string& CBinaryExp::GetTypeStr() const
{
    return TypeToStr.at( binType );
}

const IExp* CBinaryExp::GetLeft() const
{
    return leftExp.get();
}

const IExp* CBinaryExp::GetRight() const
{
    return rightExp.get();
}

CMemoryExp::CMemoryExp( const IExp* exp ) :
    exp( exp )
{
}

void CMemoryExp::Accept( IVisitorIRT* v ) const
{
    v->Visit( this );
}

const IExp* CMemoryExp::GetMem() const
{
    return exp.get();
}

void CCallExp::Accept( IVisitorIRT* v ) const
{
    v->Visit( this );
}

const IExp* CCallExp::GetFuncExp() const
{
    return funcExp.get();
}

const CExpList* CCallExp::GetArgs() const
{
    return args.get();
}

void CESeqExp::Accept( IVisitorIRT* v ) const
{
    v->Visit( this );
}

const IStm* CESeqExp::GetStm() const
{
    return stm.get();
}

const IExp* CESeqExp::GetExp() const
{
    return exp.get();
}
}

