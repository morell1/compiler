#include "CIRTStm.h"
#include "src/IRT/visitors/IVisitorIRT.h"


namespace IRT
{

void CMoveStm::Accept( IVisitorIRT* v ) const
{
    v->Visit( this );
}

const IExp* CMoveStm::GetDst() const
{
    return dst.get();
}

const IExp* CMoveStm::GetSrc() const
{
    return src.get();
}

void CExpStm::Accept( IVisitorIRT* v ) const
{
    v->Visit( this );
}

const IExp* CExpStm::GetExp() const
{
    return exp.get();
}

void CJumpStm::Accept( IVisitorIRT* v ) const
{
    v->Visit( this );
}

const CLabel& CJumpStm::GetLabel() const
{
    return target;
}

const std::map<CCJumpStm::ERelationType, const std::string> CCJumpStm::TypeToStr
    = {{ERelationType::EQ, "EQ"},
       {ERelationType::NE, "NE"},
       {ERelationType::LT, "LT"}};

void CCJumpStm::Accept( IVisitorIRT* v ) const
{
    v->Visit( this );
}

const IExp* CCJumpStm::GetLeft() const
{
    return leftExp.get();
}

const IExp* CCJumpStm::GetRight() const
{
    return rightExp.get();
}

const CLabel& CCJumpStm::GetTrueLabel() const
{
    return ifTrueLabel;
}

const CLabel& CCJumpStm::GetFalseLabel() const
{
    return ifFalseLabel;
}

const std::string& CCJumpStm::GetTypeStr() const
{
    return TypeToStr.at( relationType );
}

void CSeqStm::Accept( IVisitorIRT* v ) const
{
    v->Visit( this );
}

const IStm* CSeqStm::GetLeft() const
{
    return leftStm.get();
}

const IStm* CSeqStm::GetRight() const
{
    return rightStm.get();
}


void CLabelStm::Accept( IVisitorIRT* v ) const
{
    v->Visit( this );
}

const CLabel& CLabelStm::GetLabel() const
{
    return label;
}

}
