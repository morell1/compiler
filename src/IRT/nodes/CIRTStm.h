#pragma once
#include <memory>
#include "IStm.h"
#include "CLabel.h"
#include "CIRTExp.h"


namespace IRT
{

class CMoveStm : public IStm
{

 public:

    CMoveStm( const IExp* dst, const IExp* src ) : dst( dst ), src( src )
    {
    }

    void Accept( IVisitorIRT* v ) const override;
    const IExp* GetDst() const;
    const IExp* GetSrc() const;

 private:
    std::unique_ptr<const IExp> dst;
    std::unique_ptr<const IExp> src;
};

class CExpStm : public IStm
{

 public:

    explicit CExpStm( const IExp* exp ) : exp( exp )
    {
    }

    void Accept( IVisitorIRT* v ) const override;
    const IExp* GetExp() const;

 private:
    std::unique_ptr<const IExp> exp;
};

class CJumpStm : public IStm
{

 public:

    explicit CJumpStm( const CLabel& target_ ) : target( target_ )
    {
    }

    void Accept( IVisitorIRT* v ) const override;
    const CLabel& GetLabel() const;

 private:
    CLabel target;
};

class CCJumpStm : public IStm
{

 public:
    enum class ERelationType
    {
        EQ,
        NE,
        LT
    };

    const static std::map<ERelationType, const std::string> TypeToStr;
    const ERelationType relationType;

    CCJumpStm( ERelationType relType,
               const IExp* left,
               const IExp* right,
               const CLabel& ifTrue,
               const CLabel& ifFalse ) :
        relationType( relType ),
        leftExp( left ),
        rightExp( right ),
        ifTrueLabel( ifTrue ),
        ifFalseLabel( ifFalse )
    {
    }

    void Accept( IVisitorIRT* v ) const override;
    const std::string& GetTypeStr() const;
    const IExp* GetLeft() const;
    const IExp* GetRight() const;
    const CLabel& GetTrueLabel() const;
    const CLabel& GetFalseLabel() const;

 private:
    std::unique_ptr<const IExp> leftExp;
    std::unique_ptr<const IExp> rightExp;
    CLabel ifTrueLabel;
    CLabel ifFalseLabel;
};

class CSeqStm : public IStm
{

 public:

    CSeqStm( const IStm* left, const IStm* right ) : leftStm( left ), rightStm( right )
    {
    }

    void Accept( IVisitorIRT* v ) const override;
    const IStm* GetLeft() const;
    const IStm* GetRight() const;

 private:
    std::unique_ptr<const IStm> leftStm;
    std::unique_ptr<const IStm> rightStm;
};

class CLabelStm : public IStm
{

 public:

    explicit CLabelStm( const CLabel& label ) : label( label )
    {
    }

    void Accept( IVisitorIRT* v ) const override;
    const CLabel& GetLabel() const;

 private:
    CLabel label;
};
}
