#include <iostream>
#include <src/common.h>
#include "CGraphBuilder.h"


namespace IRT
{

CGraphBuilder::CGraphBuilder( const std::string& path ) :
    nodeNumber( 0 )
{
    f = fopen( path.c_str(), "w+" );
    if( f == nullptr ) {
        std::cout << "GraphBuilder:Error: can't open input file" << std::endl;
        throw CompilerException();
    }
    fprintf( f, "%s", "strict graph G{\n" );
}

CGraphBuilder::~CGraphBuilder()
{
    fprintf( f, "%s", "}" );
    fclose( f );
}

void CGraphBuilder::Visit( const CConstExp* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CONST\\n%d\"];\n", cur, n->GetValue() );
}

void CGraphBuilder::Visit( const CNameExp* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"NAME\\n%s\"];\n", cur, n->GetLabel().String().c_str() );
}

void CGraphBuilder::Visit( const CTempExp* n )
{
    int cur = nodeNumber;
    std::string tmpLabel = n->GetValueLabel();
    fprintf( f, "%d [label=\"TEMP\\n%s\"];\n", cur, tmpLabel.c_str() );
}

void CGraphBuilder::Visit( const CBinaryExp* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"%s\"];\n", cur, n->GetTypeStr().c_str() );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetLeft()->Accept( this );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetRight()->Accept( this );
}

void CGraphBuilder::Visit( const CMemoryExp* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"MEM\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetMem()->Accept( this );
}

void CGraphBuilder::Visit( const CCallExp* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CALL\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetFuncExp()->Accept( this );
    const auto& arguments = n->GetArgs()->GetExpressions();
    for( const auto& arg : arguments ) {
        nodeNumber++;
        fprintf( f, "%d -- %d;\n", cur, nodeNumber );
        arg->Accept( this );
    }
}

void CGraphBuilder::Visit( const CESeqExp* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"ESEQ\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetExp()->Accept( this );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetStm()->Accept( this );
}

void CGraphBuilder::Visit( const CMoveStm* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"MOVE\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetSrc()->Accept( this );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetDst()->Accept( this );
}

void CGraphBuilder::Visit( const CExpStm* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"EXP\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetExp()->Accept( this );
}

void CGraphBuilder::Visit( const CJumpStm* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"JUMP\\n%s\"];\n", cur, n->GetLabel().String().c_str() );
}

void CGraphBuilder::Visit( const CCJumpStm* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"CJUMP\\n%s\"];\n", cur, n->GetTypeStr().c_str() );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetLeft()->Accept( this );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetRight()->Accept( this );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    fprintf( f, "%d [label=\"iftrue\\n%s\"];\n", nodeNumber, n->GetTrueLabel().String().c_str() );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    fprintf( f, "%d [label=\"iffalse\\n%s\"];\n", nodeNumber, n->GetFalseLabel().String().c_str() );
}

void CGraphBuilder::Visit( const CSeqStm* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"SEQ\"];\n", cur );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetLeft()->Accept( this );
    nodeNumber++;
    fprintf( f, "%d -- %d;\n", cur, nodeNumber );
    n->GetRight()->Accept( this );
}

void CGraphBuilder::Visit( const CLabelStm* n )
{
    int cur = nodeNumber;
    fprintf( f, "%d [label=\"LABEL\\n%s\"];\n", cur, n->GetLabel().String().c_str() );
}

};
