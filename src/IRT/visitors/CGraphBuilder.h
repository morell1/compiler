#pragma once

#include "IVisitorIRT.h"


namespace IRT
{

class CGraphBuilder : public IVisitorIRT
{

 public:
    explicit CGraphBuilder( const std::string& path );
    CGraphBuilder( const CGraphBuilder& other ) = delete;
    const CGraphBuilder& operator=( const CGraphBuilder& ) = delete;
    ~CGraphBuilder();

    void Visit( const CConstExp* n ) override;
    void Visit( const CNameExp* n ) override;
    void Visit( const CTempExp* n ) override;
    void Visit( const CBinaryExp* n ) override;
    void Visit( const CMemoryExp* n ) override;
    void Visit( const CCallExp* n ) override;

    void Visit( const CESeqExp* n ) override;

    void Visit( const CMoveStm* n ) override;
    void Visit( const CExpStm* n ) override;
    void Visit( const CJumpStm* n ) override;
    void Visit( const CCJumpStm* n ) override;
    void Visit( const CSeqStm* n ) override;
    void Visit( const CLabelStm* n ) override;

 private:
    FILE* f;
    int nodeNumber;
};

}
