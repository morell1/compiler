#pragma once

#include <src/AST/visitors/IVisitor.h>
#include <src/ActivationRecords/IFrame.h>
#include <src/SymbolTable/CTable.h>
#include <src/ActivationRecords/CX86MiniJavaFrame.h>
#include "src/IRT/nodes/IStm.h"
#include "src/IRT/ISubtreeWrapper.h"


namespace IRT
{
struct CCodeFragment
{
    std::unique_ptr<const IFrame> frame;
    std::unique_ptr<const IStm> body;

    CCodeFragment( const IFrame* frame_, const IStm* body_ ) : frame( frame_ ), body( body_ )
    {
    }

    CCodeFragment( CCodeFragment&& other ) noexcept : frame( std::move( other.frame ) ), body( std::move( other.body ) )
    {
    }
};

class CTranslator : public IVisitor
{

 public:
    explicit CTranslator( CTable* symbolTable_ ) : symbolTable( symbolTable_ )
    {
    }

    std::string MakeMethodFullName( const std::string& className, const std::string& methodName )
    {
        return className + "::" + methodName;
    }

    void AddClassFields( const CClassInfo* classDefinition );
    void BuildNewFrame( CSymbol methodSymbol );
    void ProcessStmList( const std::vector<std::unique_ptr<AST::IStm>>* statements );
    CBinaryExp::EBinaryType BinaryOperatorFromAstToIr( AST::CBinaryExp::TYPE t );

    void Visit( const CGoal* n ) override;
    void Visit( const CMainClass* n ) override;
    void Visit( const CClassDeclaration* n ) override;
    void Visit( const CVarDeclaration* n ) override;
    void Visit( const CArgument* n ) override;
    void Visit( const CMethodDeclaration* n ) override;
    void Visit( const CType* n ) override;

    void Visit( const CIfStm* n ) override;
    void Visit( const CWhileStm* n ) override;
    void Visit( const CComplexStm* n ) override;
    void Visit( const CPrintStm* n ) override;
    void Visit( const CAssignmentStm* n ) override;
    void Visit( const CArrayAssignmentStm* n ) override;

    void Visit( const AST::CBinaryExp* n ) override;
    void Visit( const CIndexExp* n ) override;
    void Visit( const CLengthExp* n ) override;
    void Visit( const CMethodCallExp* n ) override;
    void Visit( const CIntegerExp* n ) override;
    void Visit( const CBooleanConstExp* n ) override;
    void Visit( const CIdentifierExp* n ) override;
    void Visit( const CThisExp* n ) override;
    void Visit( const CNewArrExp* n ) override;
    void Visit( const CNewExp* n ) override;
    void Visit( const CNegationExp* n ) override;
    void Visit( const CParenthesesExp* n ) override;

    void Visit( const CIdentifier* n ) override;
    const std::map<std::string, CCodeFragment>& GetCodeFragments() const;

 private:
    std::map<std::string, CCodeFragment> codeFragments;
    IFrame* curFrame = nullptr;
    std::unique_ptr<ISubtreeWrapper> curWrapper = nullptr;
    CTable* symbolTable;
    CClassInfo* curClass = nullptr;
    CSymbol callerClassSymbol = CSymbol::None;
    CMethodInfo* curMethod = nullptr;
};
}
