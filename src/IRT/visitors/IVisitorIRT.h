#pragma once

#include <src/IRT/nodes/CIRTExp.h>
#include <src/IRT/nodes/CIRTStm.h>


namespace IRT
{

class IVisitorIRT
{

 public:
    virtual void Visit( const CConstExp* n ) = 0;
    virtual void Visit( const CNameExp* n ) = 0;
    virtual void Visit( const CTempExp* n ) = 0;
    virtual void Visit( const CBinaryExp* n ) = 0;
    virtual void Visit( const CMemoryExp* n ) = 0;
    virtual void Visit( const CCallExp* n ) = 0;
    virtual void Visit( const CESeqExp* n ) = 0;

    virtual void Visit( const CMoveStm* n ) = 0;
    virtual void Visit( const CExpStm* n ) = 0;
    virtual void Visit( const CJumpStm* n ) = 0;
    virtual void Visit( const CCJumpStm* n ) = 0;
    virtual void Visit( const CSeqStm* n ) = 0;
    virtual void Visit( const CLabelStm* n ) = 0;
};

};
