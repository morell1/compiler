#include "CMethodInfo.h"

CVariableInfo* CMethodInfo::GetVarDeclared( CSymbol s )
{
    auto t1 = args.find( s );
    if( t1 != args.end() ) {
        return t1->second.get();
    }
    auto t2 = locals.find( s );
    if( t2 != locals.end() ) {
        return t2->second.get();
    }
    return nullptr;
}

bool CMethodInfo::HasOwnVariableDeclared( CSymbol s )
{
    if( args.find( s ) != args.end() ) {
        return true;
    }
    return locals.find( s ) != locals.end();
}

const CSymbol& CMethodInfo::GetName() const
{
    return name;
}

const CType* CMethodInfo::GetRetType() const
{
    return retType;
}

bool CMethodInfo::ArgExist( const CSymbol& argName ) const
{
    return args.find( argName ) != args.end();
}

bool CMethodInfo::LocalExist( const CSymbol& varName ) const
{
    return locals.find( varName ) != locals.end();
}

const std::map<CSymbol, std::unique_ptr<CVariableInfo>>& CMethodInfo::GetArgs() const
{
    return args;
}

const std::map<CSymbol, std::unique_ptr<CVariableInfo>>& CMethodInfo::GetLocals() const
{
    return locals;
}

void CMethodInfo::AddArgument( const CSymbol& argName, std::unique_ptr<CVariableInfo>&& varInfo )
{
    args[argName] = std::move( varInfo );
}

void CMethodInfo::AddLocal( const CSymbol& varName, std::unique_ptr<CVariableInfo>&& varInfo )
{
    locals[varName] = std::move( varInfo );
}

const CVariableInfo* CMethodInfo::GetArg( const CSymbol& argName )
{
    return args[argName].get();
}

const CVariableInfo* CMethodInfo::GetLocal( const CSymbol& varName )
{
    return locals[varName].get();
}
