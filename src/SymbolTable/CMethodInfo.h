#pragma once

#include "utils.h"
#include "CVariableInfo.h"


class CMethodInfo
{

 public:
    CMethodInfo( CSymbol name_, CType* retType_ ) :
        name( name_ ), retType( retType_ )
    {
    }

    CVariableInfo* GetVarDeclared( CSymbol s );
    bool HasOwnVariableDeclared( CSymbol s );
    bool ArgExist( const CSymbol& argName ) const;
    bool LocalExist( const CSymbol& varName ) const;
    void AddArgument( const CSymbol& argName, std::unique_ptr<CVariableInfo>&& varInfo );
    void AddLocal( const CSymbol& varName, std::unique_ptr<CVariableInfo>&& varInfo );
    const CVariableInfo* GetArg( const CSymbol& argName );
    const CVariableInfo* GetLocal( const CSymbol& varName );
    const std::map<CSymbol, std::unique_ptr<CVariableInfo>>& GetArgs() const;
    const std::map<CSymbol, std::unique_ptr<CVariableInfo>>& GetLocals() const;
    const CSymbol& GetName() const;
    const CType* GetRetType() const;

 private:
    CSymbol name;
    CType* retType;
    std::map<CSymbol, std::unique_ptr<CVariableInfo>> args;
    std::map<CSymbol, std::unique_ptr<CVariableInfo>> locals;
};
