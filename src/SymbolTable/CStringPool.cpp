#include "CStringPool.h"


CSymbol CStringPool::GetIntern( const std::string& src )
{
    auto cached = stringToSymbol.find( src );
    if( cached != stringToSymbol.end() ) {
        return CSymbol( cached->second.get() );
    }
    allStrings.push_back( src );
    auto newVal = new CSymbolInternals( allStrings.back() );
    stringToSymbol.insert( {allStrings.back(), std::unique_ptr<CSymbolInternals>( newVal )} );
    return CSymbol( newVal );
}
