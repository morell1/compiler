#pragma once
#include <string>
#include <map>
#include <list>
#include <memory>
#include "CSymbol.h"


class CStringPool
{

 public:
    CSymbol GetIntern( const std::string& src );

 private:
    std::map<std::string, std::unique_ptr<CSymbolInternals>> stringToSymbol;
    std::list<std::string> allStrings;
};
