#include "CSymbolTableBuilder.h"
#include <memory>


void CSymbolTableBuilder::Visit( const CGoal* n )
{
    n->GetMainClass()->Accept( this );
    for( auto& classDeclaration : *n->GetClassDeclarations() ) {
        classDeclaration->Accept( this );
    }
}

void CSymbolTableBuilder::Visit( const CMainClass* n )
{
    curClass = std::make_unique<CClassInfo>( n->GetId()->GetValue() );

    if( hasClass( curClass->GetName() ) ) {
        std::string error = string_format( "Class %s was already declared\nTrying to redeclare at (line %d)\n",
                                           curClass->GetName()->String().c_str(),
                                           n->fstLine );
        errors.push_back( error );
    }

    std::unique_ptr<CMethodInfo>
        mainMethod = std::make_unique<CMethodInfo>( n->GetMainId()->GetValue(), &CTable::typeVoid );
    mainMethod->AddArgument( n->GetArgsId()->GetValue(),
                             std::make_unique<CVariableInfo>( &CTable::typeStringArr, n->GetArgsId()->GetValue() ) );
    curClass->SetMethod( n->GetMainId()->GetValue(), std::move( mainMethod ) );

    table->classes[curClass->GetName()] = std::move( curClass );
}

void CSymbolTableBuilder::Visit( const CClassDeclaration* n )
{
    curClass = std::make_unique<CClassInfo>( n->GetId()->GetValue() );

    if( n->GetExtend() == nullptr ) {
        curClass->SetParent( CSymbol( nullptr ) );
        curClass->SetParentInfo( nullptr );
    } else {
        curClass->SetParent( n->GetExtend()->GetValue() );
        curClass->SetParentInfo( nullptr );
    }

    if( hasClass( curClass->GetName() ) ) {
        std::string error = string_format( "Class %s was already declared\nTrying to redeclare at (line %d)\n",
                                           curClass->GetName()->String().c_str(),
                                           n->fstLine );
        errors.push_back( error );
    }

    for( auto& varDeclaration: *( n->GetVarDeclarations() ) ) {
        varDeclaration->Accept( this );
    }
    for( auto& methodDeclaration: *( n->GetMethodDeclarations() ) ) {
        methodDeclaration->Accept( this );
    }

    // Can do it after visiting children, because populating symbol table cares only about duplicates.
    table->classes[curClass->GetName()] = std::move( curClass );
}

void CSymbolTableBuilder::Visit( const CVarDeclaration* n )
{
    curVariable = std::make_unique<CVariableInfo>( n->GetType(), n->GetId()->GetValue() );

    if( curMethod != nullptr ) {
        if( curMethod->LocalExist( curVariable->GetSymbol() ) ) {
            std::string error = string_format(
                "Variable %s was already declared as local variable of method %s.\n Trying to redeclare at (line %d)\n",
                curVariable->GetSymbol()->String().c_str(),
                curMethod->GetName()->String().c_str(),
                n->fstLine );
            errors.push_back( error );
        }
        curMethod->AddLocal( curVariable->GetSymbol(), std::move( curVariable ) );
    } else {

        if( curClass->HasField( curVariable->GetSymbol() ) ) {
            std::string error = string_format(
                "Variable %s was already declared as field of class %s.\n Trying to redeclare at (line %d)\n",
                curVariable->GetSymbol()->String().c_str(),
                curClass->GetName()->String().c_str(),
                n->fstLine );
            errors.push_back( error );
        }
        curClass->SetField( curVariable->GetSymbol(), std::move( curVariable ) );
    }
}

void CSymbolTableBuilder::Visit( const CArgument* n )
{
    curVariable = std::make_unique<CVariableInfo>( n->GetType(), n->GetId()->GetValue() );

    if( curMethod->ArgExist( curVariable->GetSymbol() ) ) {
        std::string error = string_format(
            "Variable %s was already declared as argument of method %s.\n Trying to redeclare at (line %d)\n",
            curVariable->GetSymbol()->String().c_str(),
            curMethod->GetName()->String().c_str(),
            n->fstLine );
        errors.push_back( error );
    }
    curMethod->AddArgument( curVariable->GetSymbol(), std::move( curVariable ) );
}

void CSymbolTableBuilder::Visit( const CMethodDeclaration* n )
{
    curMethod = std::make_unique<CMethodInfo>( n->GetId()->GetValue(), n->GetType() );

    if( curClass->HasMethod( curMethod->GetName() ) ) {
        std::string error = string_format(
            "Method %s was already declared in class %s.\n Trying to redeclare at (line %d)\n",
            curMethod->GetName()->String().c_str(),
            curClass->GetName()->String().c_str(),
            n->fstLine );
        errors.push_back( error );
    }

    for( auto& localVar: *n->GetLocalArguments() ) {
        localVar->Accept( this );
    }

    for( auto& arg: *n->GetArguments() ) {
        arg->Accept( this );
    }

    // Can do it after visiting children, because populating symbol table cares only about duplicates.
    curClass->SetMethod( curMethod->GetName(), std::move( curMethod ) );
}

void CSymbolTableBuilder::Visit( const CType* n )
{
    assert( false );
}

void CSymbolTableBuilder::Visit( const CComplexStm* n )
{
    assert( false );
}

void CSymbolTableBuilder::Visit( const CIfStm* n )
{
    assert( false );
}

void CSymbolTableBuilder::Visit( const CWhileStm* n )
{
    assert( false );
}

void CSymbolTableBuilder::Visit( const CPrintStm* n )
{
    assert( false );
}

void CSymbolTableBuilder::Visit( const CAssignmentStm* n )
{
    assert( false );
}

void CSymbolTableBuilder::Visit( const CArrayAssignmentStm* n )
{
    assert( false );
}

void CSymbolTableBuilder::Visit( const CBinaryExp* n )
{
    assert( false );
}

void CSymbolTableBuilder::Visit( const CIndexExp* n )
{
    assert( false );
}

void CSymbolTableBuilder::Visit( const CLengthExp* n )
{
    assert( false );
}

void CSymbolTableBuilder::Visit( const CMethodCallExp* n )
{
    assert( false );
}

void CSymbolTableBuilder::Visit( const CIntegerExp* n )
{
    assert( false );
}

void CSymbolTableBuilder::Visit( const CBooleanConstExp* n )
{
    assert( false );
}

void CSymbolTableBuilder::Visit( const CIdentifierExp* n )
{
    assert( false );
}

void CSymbolTableBuilder::Visit( const CThisExp* n )
{
    assert( false );
}

void CSymbolTableBuilder::Visit( const CNewArrExp* n )
{
    assert( false );
}

void CSymbolTableBuilder::Visit( const CNewExp* n )
{
    assert( false );
}

void CSymbolTableBuilder::Visit( const CNegationExp* n )
{
    assert( false );
}

void CSymbolTableBuilder::Visit( const CParenthesesExp* n )
{
    assert( false );
}

void CSymbolTableBuilder::Visit( const CIdentifier* n )
{
    assert( false );
}

std::unique_ptr<CTable>& CSymbolTableBuilder::BuildTable( CGoal* ast )
{
    Visit( ast );
    return table;
}

void CSymbolTableBuilder::PrintErrors()
{
    for( int i = 0; i < errors.size(); ++i ) {
        printf( "ERROR %d: %s", i, errors[i].c_str() );
    }
    if( !errors.empty() ) {
        throw CompilerException();
    }
}

bool CSymbolTableBuilder::hasClass( CSymbol className )
{
    return table->classes.find( className ) != table->classes.end();
}
