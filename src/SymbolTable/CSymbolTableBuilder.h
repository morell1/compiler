#pragma once

#include <src/common.h>
#include <cassert>
#include "src/AST/visitors/IVisitor.h"
#include "CTable.h"
#include "CMethodInfo.h"
#include "utils.h"


class CSymbolTableBuilder : public IVisitor
{

 public:
    explicit CSymbolTableBuilder() :
        table( new CTable ), curClass( nullptr ), curMethod( nullptr ), curVariable( nullptr )
    {
    }

    void Visit( const CGoal* n ) override;
    void Visit( const CMainClass* n ) override;
    void Visit( const CClassDeclaration* n ) override;
    void Visit( const CVarDeclaration* n ) override;
    void Visit( const CArgument* n ) override;
    void Visit( const CMethodDeclaration* n ) override;

    void Visit( const CType* n ) override;

    void Visit( const CIfStm* n ) override;
    void Visit( const CWhileStm* n ) override;
    void Visit( const CComplexStm* n ) override;
    void Visit( const CPrintStm* n ) override;
    void Visit( const CAssignmentStm* n ) override;
    void Visit( const CArrayAssignmentStm* n ) override;

    void Visit( const CBinaryExp* n ) override;
    void Visit( const CIndexExp* n ) override;
    void Visit( const CLengthExp* n ) override;
    void Visit( const CMethodCallExp* n ) override;
    void Visit( const CIntegerExp* n ) override;
    void Visit( const CBooleanConstExp* n ) override;
    void Visit( const CIdentifierExp* n ) override;
    void Visit( const CThisExp* n ) override;
    void Visit( const CNewArrExp* n ) override;
    void Visit( const CNewExp* n ) override;
    void Visit( const CNegationExp* n ) override;
    void Visit( const CParenthesesExp* n ) override;

    void Visit( const CIdentifier* n ) override;

    std::unique_ptr<CTable>& BuildTable( CGoal* ast );
    void PrintErrors();

 private:
    std::unique_ptr<CTable> table;
    std::unique_ptr<CClassInfo> curClass;
    std::unique_ptr<CMethodInfo> curMethod;
    std::unique_ptr<CVariableInfo> curVariable;
    std::vector<std::string> errors;

    // Возвращает true,если className есть в таблице
    bool hasClass( CSymbol className );
};
