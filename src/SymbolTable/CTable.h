#pragma once

#include <map>
#include <vector>
#include "CClassInfo.h"


class CTable
{

 public:
    std::map<CSymbol, std::unique_ptr<CClassInfo>> classes;
    static CType typeVoid;
    static CType typeStringArr;
};
