#pragma once
#include "src/AST/nodes/CType.h"


class CTypeInfo
{

 public:
    static CTypeInfo Int;
    static CTypeInfo Boolean;
    static CTypeInfo IntArr;

    explicit CTypeInfo( CType::TYPE type_, CSymbol name_ = CSymbol::None ) :
        type( type_ ), name( name_ )
    {
    }
    explicit CTypeInfo( const CType& t );

    std::string GetString();
    CType::TYPE GetType() const;
    const CSymbol& GetName() const;
    bool operator==( const CTypeInfo& other ) const;
    bool operator!=( const CTypeInfo& other ) const;

 private:
    CType::TYPE type;
    CSymbol name;
};
