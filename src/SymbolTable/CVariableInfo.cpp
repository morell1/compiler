#include "CVariableInfo.h"


const CType* CVariableInfo::GetType() const
{
    return type;
}

const CSymbol& CVariableInfo::GetSymbol() const
{
    return symbol;
}