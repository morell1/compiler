#pragma once

#include <src/SymbolTable/CSymbol.h>
#include <src/AST/nodes/CType.h>


class CVariableInfo
{

 public:
    CVariableInfo() = delete;

    CVariableInfo( const CType* type_, const CSymbol symbol_ ) :
        type( type_ ), symbol( symbol_ )
    {
    }

    const CType* GetType() const;
    const CSymbol& GetSymbol() const;

 private:
    const CType* type;
    const CSymbol symbol;
};
