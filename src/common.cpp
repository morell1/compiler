#include "common.h"


std::string GetPureFileName( const fs::path& path )
{
    std::regex rgx( "(\\w+).java" );
    std::smatch match;
    const std::string tmp = path.filename().generic_string();
    std::regex_search( tmp.begin(), tmp.end(), match, rgx );
    return match[1];
}
