#include <cstdlib>
#include <iostream>
#include <regex>

#include "compiler.h"
#include "common.h"

#include "src/AST/visitors/COneLinePrinter.h"
#include "src/AST/visitors/GraphBuilder.h"
#include "src/IRT/visitors/CGraphBuilder.h"
#include "src/SymbolTable/CSymbolTableBuilder.h"
#include "src/SymbolTable/CTypeChecker.h"
#include <src/IRT/visitors/CTranslator.h>

#include "parser.hpp"
#include "lexer.h"

//#include <src/IRT/nodes/CLabel.h>

// cppcheck-suppress uninitMemberVarPrivate
Compiler::Compiler()
{
    IRT::CLabel::ResetCounter();
    IRT::CLabel::ResetCounter();
    CSymbolInternals::ResetCounter();
}

Compiler::Compiler( TCompilePhase phase,
                    FILE* input,
                    FILE* output,
                    bool debug,
                    const std::string& name,
                    const fs::path& outputFilePath ) :
    input( input ),
    output( output ),
    inputFilePath( std::move( name ) ),
    outputPath( outputFilePath ),
    phase( phase ),
    debug( debug )
{
    IRT::CLabel::ResetCounter();
    IRT::CLabel::ResetCounter();
    CSymbolInternals::ResetCounter();
}

std::string Compiler::GetPureFileName()
{
    std::regex rgx( "(\\w+).java" );
    std::smatch match;
    const std::string tmp = inputFilePath.filename().generic_string();
    std::regex_search( tmp.begin(), tmp.end(), match, rgx );
    return match[1];
}

int Compiler::GetBegin() const
{
    return begin;
}

CStringPool& Compiler::GetStringPool()
{
    return stringPool;
}

FILE* Compiler::GetOutput()
{
    return output;
}

TCompilePhase Compiler::GetPhase() const
{
    return phase;
}

void Compiler::ShiftBegin( int shift )
{
    begin += shift;
}

void Compiler::SetAst( CGoal* goal )
{
    ast.reset( goal );
}

void Compiler::initIORoutines( const boost::program_options::variables_map& vm )
{
    inputFilePath = vm["input-file"].as<std::string>().c_str();
    input = fopen( inputFilePath.c_str(), "r+" );
    if( input == nullptr ) {
        std::cout << "Error: can't open input file" << std::endl;
        throw CompilerException();
    }

    if( phase == TCompilePhase::PHASE_IRT ) {
        if( !vm.count( "output-file" ) ) {
            outputPath = GetPureFileName() + ":IRT";
        } else {
            outputPath = vm["output-file"].as<std::string>().c_str();
        }
        output = nullptr;
    } else {
        if( !vm.count( "output-file" ) ) {
            output = stdout;
        } else {
            output = fopen( vm["output-file"].as<std::string>().c_str(), "w+" );
        }
        if( output == nullptr ) {
            std::cout << "Error: can't open output file" << std::endl;
            throw CompilerException();
        }
    }
}

void Compiler::Compile()
{
    begin = 0;
    yyscan_t scanner;

    if( yylex_init( &scanner ) ) {
        throw CompilerException();
    }
    yyset_extra( this, scanner );

    yyset_in( input, scanner );
    if( phase == TCompilePhase::PHASE_LEX ) {
        YYSTYPE stype;
        YYLTYPE ltype;
        while( yylex( &stype, &ltype, scanner ) != EOF_TOKEN ) {}
    }
    if( phase >= TCompilePhase::PHASE_PARSE ) {
        if( phase == TCompilePhase::PHASE_PARSE ) {
            yydebug = debug;
        }

        if( yyparse( scanner, this ) ) {
            yylex_destroy( scanner );
            throw CompilerException();
        }
        if( errorsOccurred() ) {
            yylex_destroy( scanner );
            throw CompilerException();
        }
    }
    yylex_destroy( scanner );

    if( phase == TCompilePhase::PHASE_AST ) {
        if( debug ) {
            AST::COneLinePrinter pp( output );
            pp.Visit( ast.get() );
        } else {
            AST::CGraphBuilder builder( output );
            builder.Visit( ast.get() );
        }
    }
    if( phase >= TCompilePhase::PHASE_SYMBOLTABLE ) {
        CSymbolTableBuilder stb;
        symbolTable = std::move( stb.BuildTable( ast.get() ) );
        stb.PrintErrors();
        CTypeChecker tc( symbolTable.get() );
        tc.Visit( ast.get() );
        tc.printErrors();
    }

    if( phase >= TCompilePhase::PHASE_IRT ) {
        IRT::CTranslator t( symbolTable.get() );
        t.Visit( ast.get() );

        if( phase == TCompilePhase::PHASE_IRT && !debug ) {
            std::string dirname = outputPath.generic_string();
            if( !fs::exists( dirname ) ) {
                if( !fs::create_directory( dirname ) ) {
                    throw CompilerException();
                }
            }
            for( auto& codeFragment : t.GetCodeFragments() ) {
                IRT::CGraphBuilder builder( string_format( "%s/%s.dot",
                                                           dirname.c_str(),
                                                           codeFragment.second.frame->GetName().c_str() ) );
                codeFragment.second.body->Accept( &builder );
            }
        }
    }
}

Compiler Compiler::CompilerFromArgs( int argc, char** argv )
{
    Compiler c;

    namespace po = boost::program_options;
    po::options_description options( "Allowed options" );
    options.add_options()
        ( "help,h", "show help" )
        ( "debug", "enable debug output" )
        ( "lexer,l", "lex phase (outputs tokens)" )
        ( "parser,p", "parse phase (outputs nothing, bison verbose on --debug)" )
        ( "ast", "abstract syntax tree (outputs .dot file, oneline representation on --debug)" )
        ( "symboltable", "symbol table" )
        ( "irt",
          "intermediate representation tree (-o flag specifies the directory in which irt images will be placed )" )
        ( "input-file", po::value<std::string>()->required(), "input file" )
        ( "output-file,o", po::value<std::string>(), "output file" );
    po::positional_options_description positional;
    positional.add( "input-file", -1 );
    po::variables_map vm;

    try {
        po::store( po::command_line_parser( argc, argv ).options( options ).positional( positional ).run(), vm );
    }
    catch( duplicationException& exception ) {
        std::cout << "Error: arguments duplication" << std::endl;
        throw CompilerException();
    }

    if( vm.count( "help" ) ) {
        std::cout << options << "\n";
        exit( 1 );
    }

    po::notify( vm );
    if( vm.count( "parser" )
        + vm.count( "lexer" )
        + vm.count( "ast" )
        + vm.count( "symboltable" )
        + vm.count( "irt" ) > 1 ) {

        std::cout << "Error: more than one phase can't be specified" << std::endl;
        throw CompilerException();
    }

    if( vm.count( "lexer" ) ) {
        c.phase = TCompilePhase::PHASE_LEX;
    } else if( vm.count( "parser" ) ) {
        c.phase = TCompilePhase::PHASE_PARSE;
    } else if( vm.count( "ast" ) ) {
        c.phase = TCompilePhase::PHASE_AST;
    } else if( vm.count( "symboltable" ) ) {
        c.phase = TCompilePhase::PHASE_SYMBOLTABLE;
    } else if( vm.count( "irt" ) ) {
        c.phase = TCompilePhase::PHASE_IRT;
    } else {
        c.phase = TCompilePhase::PHASE_ALL;
    }

    c.debug = vm.count( "debug" ) != 0;
    c.initIORoutines( vm );
    return c;
}

void Compiler::AddErrorMessage( const std::string& errMsg )
{
    errors.push_back( errMsg );
}

void Compiler::PrintErrors() const
{
    std::cout << "Encountered errors:" << '\n';
    for( int i = 0; i < errors.size(); i++ ) {
        std::cout << errors[i] << std::endl;
    }
}

bool Compiler::errorsOccurred() const
{
    return !errors.empty();
}
