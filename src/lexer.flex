%{
#include "parser.hpp"
#include "src/common.h"

extern void yyerror(YYLTYPE* yylloc, yyscan_t scanner, Compiler* compiler, const char *msg);

#define debug_lexer(arg) \
if (yyextra->GetPhase() == TCompilePhase::PHASE_LEX) { \
    arg; \
}

#define if_parse(arg) \
if (yyextra->GetPhase() > TCompilePhase::PHASE_LEX) { \
    arg; \
}

#define print_token_lexer(name, ...) \
        debug_lexer(fprintf(yyextra->GetOutput(), "%s"  __VA_OPT__("(%*s)") "(%d,%d) ", name, __VA_ARGS__ __VA_OPT__(,) yyextra->GetBegin(), yyextra->GetBegin()+yyleng)); \
        yyextra->ShiftBegin(yyleng);

int column = 1;
#define YY_USER_ACTION yylloc->first_line = yylloc->last_line = yylineno; \
    yylloc->first_column = column; yylloc->last_column = column + yyleng; \
    column += yyleng;
%}

/*%option prefix="mj"*/
/*%option outfile="MiniJava.Lexer.cpp" header-file="MiniJava.Lexer.h"*/
/* %option c++ */

/* %option warn nodefault */
%option noyywrap nounput noinput never-interactive nounistd
%option reentrant
%option extra-type="Compiler*"
%option bison-bridge
%option bison-locations
%option yylineno

DIGIT		[0-9]
LETTER		[a-zA-Z_]
ID			{LETTER}({DIGIT}|{LETTER})*
NUM			[1-9]{DIGIT}*|0
SPACE		(" "|"\v"|"\r")+
COMMENT		"//".*""

%%
{COMMENT}"\n" { column = 1; yyextra->ShiftBegin(yyleng);}
"\n" { column = 1; }
"\t" { column += 3; }

"." { print_token_lexer("DOT"); return DOT; }
"," { print_token_lexer("COMMA"); return COMMA; }
";" { print_token_lexer("SEMI"); return SEMI; }

"{" { print_token_lexer("LBRACE"); return LBRACE; }
"}" { print_token_lexer("RBRACE"); return RBRACE; }
"[" { print_token_lexer("LBRACKET"); return LBRACKET; }
"]" { print_token_lexer("RBRACKET"); return RBRACKET; }
"(" { print_token_lexer("LPARENTH"); return LPARENTH; }
")" { print_token_lexer("RPARENTH"); return RPARENTH; }

"true" { print_token_lexer("LOGICAL_TRUE"); return LOGICAL_TRUE; }
"false" { print_token_lexer("LOGICAL_FALSE"); return LOGICAL_FALSE; }
"this" { print_token_lexer("THIS"); return THIS; }
"new" { print_token_lexer("NEW"); return NEW; }
"int" { print_token_lexer("INT"); return INT; }
"if" { print_token_lexer("IF"); return IF; }
"else" { print_token_lexer("ELSE"); return ELSE; }
"while" { print_token_lexer("WHILE"); return WHILE; }
"System.out.println" { print_token_lexer("PRINT"); return PRINT; }
"class" { print_token_lexer("CLASS"); return CLASS; }
"public" { print_token_lexer("PUBLIC"); return PUBLIC; }
"static" { print_token_lexer("STATIC"); return STATIC; }
"void" { print_token_lexer("VOID"); return VOID; }
"main" { print_token_lexer("MAIN"); return MAIN; }
"String" { print_token_lexer("STRING"); return STRING; }
"extends" { print_token_lexer("EXTENDS"); return EXTENDS; }
"private" { print_token_lexer("PRIVATE"); return PRIVATE; }
"length" { print_token_lexer("LENGTH"); return LENGTH; }
"boolean" { print_token_lexer("BOOLEAN"); return BOOLEAN; }
"return" { print_token_lexer("RETURN"); return RETURN; }

"+" { print_token_lexer("PLUS"); return PLUS; }
"-" { print_token_lexer("MINUS"); return MINUS; }
"*" { print_token_lexer("MUL"); return MUL; }
"/" { print_token_lexer("DIV"); return DIV; }
"=" { print_token_lexer("EQ"); return EQ; }
"&&" { print_token_lexer("LOGICAL_AND"); return LOGICAL_AND; }
"<" { print_token_lexer("LESS"); return LESS; }
"!" { print_token_lexer("EXCL_MARK"); return EXCL_MARK; }
<<EOF>> {print_token_lexer("EOF_TOKEN"); return EOF_TOKEN; }
{ID} { if_parse(yylval->str = strdup(yytext); print_token_lexer("ID", yyleng, yytext);) return ID; }
{NUM} { if_parse(yylval->num = atoi(yytext); print_token_lexer("NUM", yyleng, yytext);) return NUM; }
{SPACE} { yyextra->ShiftBegin(yyleng); }

. { yyerror(yylloc_param, yyscanner, reinterpret_cast<Compiler*>(yyextra), string_format("Unknown \"%*s\" (chr %d, 0x%02X) at %d", yyleng, yytext, yytext[0]+256, yytext[0]+256, yyextra->GetBegin()).c_str()); debug_lexer(yylex_destroy( yyscanner ); throw CompilerException();) return ERROR_TOKEN; }
%%
