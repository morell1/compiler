#include "compiler.h"


int main( int argc, char* argv[] )
{
    Compiler&& compiler = Compiler::CompilerFromArgs( argc, argv );
    try {
        compiler.Compile();
    } catch( CompilerException& ex ) {
        compiler.PrintErrors();
    }

    return 0;
}
