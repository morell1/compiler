#pragma once
#include <vector>
#include <string>
#include <regex>
#include <fstream>
#include <sstream>


std::vector<int> get_true_error_lines( const std::string& filename );

std::vector<int> get_result_error_lines( const std::string& re_string, const std::string& output );
