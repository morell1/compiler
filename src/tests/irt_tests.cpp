#include <fstream>
#include <regex>
#include "gtest/gtest.h"
#include "../compiler.h"
#include "../common.h"
#include "string_reformers.h"


class TestIRTreeWorks : public testing::TestWithParam<std::string>
{
 public:
    const static std::vector<std::string> files;
};

const std::vector<std::string>TestIRTreeWorks::files =
    {"Samples/BinarySearch.java", "Samples/BinaryTree.java", "Samples/BubbleSort.java", "Samples/Factorial.java",
     "Samples/LinearSearch.java", "Samples/LinkedList.java", "Samples/QuickSort.java", "Samples/TreeVisitor.java"
    };

INSTANTIATE_TEST_CASE_P( Tests,
                         TestIRTreeWorks,
                         ::testing::ValuesIn( TestIRTreeWorks::files ) );

TEST_P( TestIRTreeWorks, IRTreeSamples )
{
    const fs::path rootDirectory = "Samples/IRT";

    if( !fs::is_directory( rootDirectory / "Actual" ) || !fs::exists( rootDirectory / "Actual" ) ) {
        fs::create_directory( rootDirectory / "Actual" );
    }

    std::string filename = GetParam();
    FILE* input = fopen( filename.c_str(), "r+" );
    if( input == nullptr ) {
        throw std::ifstream::failure( "TEST_P : IRTreeSamples: open input failed" );
    }
    Compiler compiler( TCompilePhase::PHASE_IRT, input, stdout, false, filename,
                       rootDirectory / "Actual" / ( GetPureFileName( filename ) ) );

    testing::internal::CaptureStdout();
    try {
        compiler.Compile();
    }
    catch( CompilerException& ex ) {
        fclose( input );
        testing::internal::GetCapturedStdout();
        throw;
    }
    catch( ... ) {
        fclose( input );
        testing::internal::GetCapturedStdout();
        throw CompilerException();
    }
    fclose( input );
    testing::internal::GetCapturedStdout();

    std::string sampleName = compiler.GetPureFileName();
    for( auto& file: fs::directory_iterator( rootDirectory / "Expected" / sampleName ) ) {
        auto functionName = file.path().filename().generic_string();
        if( functionName.compare( functionName.size() - 4, 4, ".dot" ) ) {
            continue;
        }
        std::string pathToExpected = rootDirectory / "Expected" / sampleName / functionName;
        std::string pathToActual = rootDirectory / "Actual" / sampleName / functionName;

        std::ifstream expected( pathToExpected.c_str() );
        std::ifstream actual( pathToActual.c_str() );

        std::string expectedEdge;
        std::string actualEdge;

        while( std::getline( expected, expectedEdge )
            && std::getline( actual, actualEdge ) ) {
            ASSERT_EQ( "Function: " + functionName + "  Edge: " + expectedEdge,
                       "Function: " + functionName + "  Edge: " + actualEdge );
        }
    }
}
