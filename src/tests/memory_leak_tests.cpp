#include <fstream>
#include "gtest/gtest.h"
#include "../compiler.h"


class TestNoLeaks : public testing::TestWithParam<std::string>
{
 public:
    const static std::vector<std::string> files;
};

const std::vector<std::string>TestNoLeaks::files =
    {"Samples/BinarySearch.java", "Samples/BinaryTree.java", "Samples/BubbleSort.java", "Samples/Factorial.java",
     "Samples/LinearSearch.java", "Samples/LinkedList.java", "Samples/QuickSort.java", "Samples/TreeVisitor.java"
    };

INSTANTIATE_TEST_CASE_P( Tests,
                         TestNoLeaks,
                         ::testing::ValuesIn( TestNoLeaks::files ) );

TEST_P( TestNoLeaks, IRTreeSamples )
{
    std::string filename = GetParam();
    FILE* input = fopen( filename.c_str(), "r+" );
    if( input == nullptr ) {
        throw std::ifstream::failure( "TEST_P : IRTreeSamples: open input failed" );
    }
    Compiler compiler( TCompilePhase::PHASE_IRT, input, stdout, true, filename );

    testing::internal::CaptureStdout();
    try {
        compiler.Compile();
    }
    catch( CompilerException& ex ) {
        fclose( input );
        testing::internal::GetCapturedStdout();
        throw;
    }
    catch( ... ) {
        fclose( input );
        testing::internal::GetCapturedStdout();
        throw CompilerException();
    }
    testing::internal::GetCapturedStdout();
    fclose( input );
}

int main( int argc, char** argv )
{
    ::testing::InitGoogleTest( &argc, argv );
    return RUN_ALL_TESTS();
}
