#pragma once

#include <string>
#include <algorithm>
#include <locale>
#include <regex>


std::string& ltrim( std::string& str );

std::string& rtrim( std::string& str );

std::string& trim( std::string& str );

std::string shrink_spaces( const std::string& s );

std::string rm_spaces_before( const std::string& s );

std::string rm_spaces_after( const std::string& s );

std::string add_spaces_before( const std::string& s );

std::string add_spaces_after( const std::string& s );

std::string get_oneline( const std::string& filename );
